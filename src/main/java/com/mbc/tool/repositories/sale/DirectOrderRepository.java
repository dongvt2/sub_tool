package com.mbc.tool.repositories.sale;

import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.model.sale.DirectOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface DirectOrderRepository extends JpaRepository<DirectOrder,String> {

    Optional<DirectOrder> findByImSerial(String imSerial);
}
