package com.mbc.tool.repositories.sale;

import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.model.sale.RetailOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RetailOrderRepository extends JpaRepository<RetailOrder,String> {

    Optional<RetailOrder> findBySimSerial(String imsi);
}
