package com.mbc.tool.repositories.subscriber;

import com.mbc.tool.model.sale.DirectOrder;
import com.mbc.tool.model.subscriber.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface SubscriberRepositoties extends JpaRepository<Subscriber,Long> {

        Optional<Subscriber> findByIsdn(String isdn);

}
