package com.mbc.tool.repositories.cust;

import com.mbc.tool.model.cust.Customer;
import com.mbc.tool.model.subscriber.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepositoties extends JpaRepository<Customer,Long> {

}
