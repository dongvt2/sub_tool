package com.mbc.tool.repositories.inventory;

import com.mbc.tool.model.inventory.Sim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface SimRepository extends JpaRepository<Sim,String> {
    Optional<Sim> findByImsi(String imsi);
    Optional<Sim> findByImSerial(String imSerial);
}
