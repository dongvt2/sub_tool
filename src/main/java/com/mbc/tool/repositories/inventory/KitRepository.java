package com.mbc.tool.repositories.inventory;

import com.mbc.tool.model.inventory.Kit;
import com.mbc.tool.model.inventory.Sim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KitRepository extends JpaRepository<Kit,String> {
    Optional<Kit> findByIsdn(String isdn);
}
