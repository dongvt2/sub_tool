package com.mbc.tool.bean;

import com.mbc.tool.rest.base.Request;
import com.mbc.tool.rest.base.Response;
import com.mbc.tool.rest.base.Validator;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.chain.impl.ContextBase;

import java.util.HashMap;

@Getter
@Setter
public class ProcessContext  extends ContextBase {
    Request request;
    Response response;
    Validator.Result result;

    //các bien phat sinh
    Object object;
}
