package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.cmp.bean.CmpSubscriberCancelInput;
import com.mbc.tool.cmp.bean.HlrResponse;
import com.mbc.tool.cmp.service.CmpRequest;
import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.repositories.inventory.SimRepository;
import com.mbc.tool.rest.base.*;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HlrSubscriberCancel4g implements Command {


    @Autowired
    CmpRequest cmpRequest;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();

        String imsi = request.getImsi();
        String msisdn = response.getMsisdn();
        String a4ki = response.getA4ki();
        String iccid = response.getIccid();
        CmpSubscriberCancelInput input = new CmpSubscriberCancelInput(msisdn,imsi,iccid,a4ki);
        HlrResponse res =  cmpRequest.hlrSubscriberCancel4G(input);
        if(res!= null){

        }
        ctx.setResult(result);
        return !result.isOk();
    }
}
