package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.model.sale.DirectOrder;
import com.mbc.tool.model.sale.RetailOrder;
import com.mbc.tool.repositories.inventory.SimRepository;
import com.mbc.tool.repositories.sale.DirectOrderRepository;
import com.mbc.tool.repositories.sale.RetailOrderRepository;
import com.mbc.tool.rest.base.*;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriberCancelUpdateSaleData implements Command {

    @Autowired
    RetailOrderRepository retailOrderRepository;

    @Autowired
    DirectOrderRepository directOrderRepository;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();

        String imSerial = response.getSimSerial();
        // update retail_order
        RetailOrder retailOrder = retailOrderRepository.findBySimSerial(imSerial).orElse(null);
        if(retailOrder != null){
            retailOrder.setStatus(3L);
            retailOrderRepository.save(retailOrder);
        }else{
            // update direct_order
            DirectOrder directOrder =  directOrderRepository.findByImSerial(imSerial).orElse(null);
            if(directOrder != null){
                directOrder.setStatusId(7L);
            }else{
                result =  new SimpleResult(ResponseCode.ERR_IM_SERIAL_INVALID.getMessage(), false,ResponseCode.ERR_IM_SERIAL_INVALID.getErrorCode());
            }
        }

        ctx.setResult(result);
        return !result.isOk();
    }
}
