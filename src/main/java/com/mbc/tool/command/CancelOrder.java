package com.mbc.tool.command;

import com.mbc.tool.chainofresponsibility.SubscriberCancel;
import com.mbc.tool.rest.SubscriberCancelRequest;
import com.mbc.tool.rest.base.Validator;

public class CancelOrder extends SubscriberCancel {

    @Override
    protected boolean canHander(Validator.Result result) {
        return false;
    }

    @Override
    protected Validator.Result checkHlr() {
        return null;
    }

    @Override
    protected Validator.Result cancleCmp() {
        return null;
    }

    @Override
    protected Validator.Result cancleHlr() {
        return null;
    }
}
