package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.Utils.Utility;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.cmp.bean.HlrInfo;
import com.mbc.tool.cmp.bean.HlrResponse;
import com.mbc.tool.cmp.service.CmpRequest;
import com.mbc.tool.rest.base.*;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

@Service
public class GetUserInfoHLRHSS implements Command {

    @Autowired
    CmpRequest cmpRequest;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();
        String msisdn = "";
        HlrResponse hlrInfo = cmpRequest.getInfoHrlHss(request.getImsi());
        if (hlrInfo != null && hlrInfo.getWsResponse() != null && !hlrInfo.getWsResponse().getHlrResponse().isEmpty() ) {
            for (HlrInfo info : hlrInfo.getWsResponse().getHlrResponse()) {
                if(info.getDetail() != null){
                    String hlrDetail = info.getDetail().getResp();
                    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    InputSource src = new InputSource();
                    src.setCharacterStream(new StringReader(hlrDetail));
                    Document doc = builder.parse(src);
                    msisdn = doc.getElementsByTagName("msisdn").item(0).getTextContent();
                    if(!Utility.isNull(msisdn)){
                        AppLog.info("MSISDN ::" + msisdn);
                        response.setMsisdn(msisdn);
                        break;
                    }
                }
            }
        }
        if(Utility.isNull(msisdn)){
            result =  new SimpleResult(ResponseCode.ERR_MSISDN.getMessage(), false,ResponseCode.ERR_MSISDN.getErrorCode());
        }

        ctx.setResult(result);
        return !result.isOk();
    }
}
