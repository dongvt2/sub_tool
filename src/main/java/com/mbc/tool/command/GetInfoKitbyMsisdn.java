package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.cmp.bean.CmpSubscriberCancelInput;
import com.mbc.tool.cmp.bean.HlrResponse;
import com.mbc.tool.cmp.service.CmpRequest;
import com.mbc.tool.model.inventory.Kit;
import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.repositories.inventory.KitRepository;
import com.mbc.tool.repositories.inventory.SimRepository;
import com.mbc.tool.rest.base.*;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetInfoKitbyMsisdn implements Command {


    @Autowired
    KitRepository kitRepository;

    @Autowired
    SimRepository simRepository;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();

       String msisdn = request.getMsisdn();

        Kit kit = kitRepository.findByIsdn(msisdn).orElse(null);
        if(kit != null){
            String imSerial = kit.getImSerial();
            Sim simInfo =  simRepository.findByImSerial(imSerial).orElse(null);
            if(simInfo != null){
                response.setMsisdn(msisdn);
                response.setIccid(simInfo.getIccid());
                response.setA4ki(simInfo.getKi());
                response.setSimSerial(simInfo.getImSerial());
                request.setImsi(simInfo.getImsi());
            }
        }else{
            result = new SimpleResult(ResponseCode.MSISDN_INVALID.getMessage(), false,ResponseCode.MSISDN_INVALID.getErrorCode());
        }

        ctx.setResult(result);
        return !result.isOk();
    }
}
