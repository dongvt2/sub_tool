package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.model.inventory.Kit;
import com.mbc.tool.repositories.inventory.KitRepository;
import com.mbc.tool.repositories.sale.DirectOrderRepository;
import com.mbc.tool.rest.base.Request;
import com.mbc.tool.rest.base.Response;
import com.mbc.tool.rest.base.Validator;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriberCancelUpdateCustomerData implements Command {

    @Autowired
    KitRepository kitRepository;

    @Autowired
    DirectOrderRepository directOrderRepository;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();

       Kit kit = kitRepository.findByIsdn(response.getMsisdn()).orElse(null);
       if(kit != null){
           kit.setStatusId(3L);
           kitRepository.save(kit);
       }

        ctx.setResult(result);
        return !result.isOk();
    }
}
