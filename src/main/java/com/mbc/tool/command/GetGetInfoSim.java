package com.mbc.tool.command;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.Utils.Utility;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.cmp.bean.HlrInfo;
import com.mbc.tool.cmp.bean.HlrResponse;
import com.mbc.tool.cmp.service.CmpRequest;
import com.mbc.tool.model.inventory.Sim;
import com.mbc.tool.repositories.inventory.SimRepository;
import com.mbc.tool.rest.base.*;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

@Service
public class GetGetInfoSim implements Command {

    @Autowired
    SimRepository simRepository;

    @Override
    public boolean execute(Context context) throws Exception {
        AppLog.info(((ProcessContext) context).getRequest().getReference() + " " + this.getClass().getSimpleName());
        ProcessContext ctx = (ProcessContext) context;
        Validator.Result result = Validator.Result.OK;
        Request request = ctx.getRequest();
        Response response = ctx.getResponse();
        String imsi = request.getImsi();
        Sim simInfo =simRepository.findByImsi(imsi).orElse(null);
        if(simInfo != null){
            response.setIccid(simInfo.getIccid());
            response.setA4ki(simInfo.getKi());
            response.setSimSerial(simInfo.getImSerial());
        }else{
            result =  new SimpleResult(ResponseCode.ERR_SIM_INFO_INVALID.getMessage(), false,ResponseCode.ERR_SIM_INFO_INVALID.getErrorCode());
        }
        ctx.setResult(result);
        return !result.isOk();
    }
}
