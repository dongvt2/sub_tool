package com.mbc.tool.service.base;

import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.rest.base.Request;
import com.mbc.tool.rest.base.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class ServiceBase {
//    @Autowired
//    TransactionLogService logService;

    public ProcessContext loadContext(Request request, Object object) {
        Response response = new Response();
        ProcessContext ctx = new ProcessContext();
        ctx.setRequest(request);
        ctx.setObject(object);
        ctx.setResponse(response);
        return ctx;
    }
}
