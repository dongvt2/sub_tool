package com.mbc.tool.service.base;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.Utils.Utility;
import com.mbc.tool.bean.ProcessContext;
import com.mbc.tool.rest.SubscriberCancelResponse;
import com.mbc.tool.rest.base.Request;
import com.mbc.tool.rest.base.Response;
import com.mbc.tool.rest.base.Validator;
import com.mbc.tool.service.SubscriberCancelByImsiService;
import com.mbc.tool.service.SubscriberCancelByMsisdnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubcriberServiceBase extends ServiceBase{
    ProcessContext ctx = null;
    public ProcessContext getContext() {
        return ctx;
    }
    @Autowired
    SubscriberCancelByImsiService subscriberCancelService;

    @Autowired
    SubscriberCancelByMsisdnService cancelByMsisdnService;

    public SubscriberCancelResponse subcriberCancel(Request request, Object object) {
        ProcessContext ctx = loadContext(request, object);
        try {
            if(Utility.isNull(request.getImsi())){
                cancelByMsisdnService.execute(ctx);
            }else{
                subscriberCancelService.execute(ctx);
            }
//            logService.execute(ctx);
        } catch (Exception ex) {
            AppLog.error(ex);
            ctx.setResult(Validator.Result.UNKNOWN);
        }
        Response response = ctx.getResponse();
        Validator.Result result = ctx.getResult();
        SubscriberCancelResponse resp = new SubscriberCancelResponse();
        if (result.isOk()) {
        }
        resp.setResult(result);
        return resp;
    }
}
