package com.mbc.tool.service;

import com.mbc.tool.command.*;
import org.apache.commons.chain.impl.ChainBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class SubscriberCancelByMsisdnService extends ChainBase {

    @Autowired
    GetInfoKitbyMsisdn getInfoKitbyMsisdn;

    @Autowired
    CmpSubscriberCancel cmpSubscriberCancel;

    @Autowired
    HlrSubscriberCancel hlrSubscriberCancel;

    @Autowired
    HlrSubscriberCancel4g hlrSubscriberCancel4g;

    @Autowired
    SubscriberCancelUpdateSaleData subscriberCancelUpdateSaleData;

    @PostConstruct
    public void addCommandChain() {
        addCommand(getInfoKitbyMsisdn);
        addCommand(cmpSubscriberCancel);
        addCommand(hlrSubscriberCancel);
        addCommand(hlrSubscriberCancel4g);
        addCommand(subscriberCancelUpdateSaleData);
    }

}
