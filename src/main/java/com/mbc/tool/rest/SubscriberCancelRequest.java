package com.mbc.tool.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscriberCancelRequest {
    private String imsi;
    private String msisdn;
}
