package com.mbc.tool.rest.base;

public enum ResponseCode {

    SUCCESS("Success","00"),
    NULL("Nukk","GW-NUll"),
    UNKNOWN("UNKNOWN ERROR","999"),
    INVALID_INPUT("INVALID_INPUT","GW100"),
    ERR_MSISDN("Msisdn invalid","GW101"),
    ERR_SIM_INFO_INVALID("Info sim invalid","GW102"),
    ERR_IM_SERIAL_INVALID("imserial invalid","GW103"),
    MSISDN_INVALID("msisdn invalid","GW104"),
    ;

    private ResponseCode(String message, String errorCode) {

        this.message = message;
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    String errorCode;
    String message;
}
