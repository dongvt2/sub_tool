package com.mbc.tool.rest.base;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SimpleResult  implements Validator.Result {

    boolean result;
    String message;
    String responseCode;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public SimpleResult(String message, boolean result, String responseCode) {
        this.result = result;
        this.message = message;
        this.responseCode = responseCode;
    }

    @Override
    public boolean isOk() {
        return result;
    }

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public String getClearMessage() {
        return message;
    }
}
