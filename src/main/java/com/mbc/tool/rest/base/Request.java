package com.mbc.tool.rest.base;

import com.mbc.tool.Utils.AppLog;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Request {
    private String service_code;
    private String mobile;
    private String amount;
    private String trace;
    private String sessionId;
    private String reference;
    private String requestId;
    private Date startRequest;
    private String otp;
    private String trans_type;
    //exchage rate
    private String urlPath;
    private String requestContent;
    private String partnerId;
    private String ipAddress;

    private String headerAgent;
    private String ipServer;

    private String imsi;
    private String msisdn;


    public Request() {
        try {
            requestId = UUID.randomUUID().toString();
            startRequest = new Date();
        } catch (Exception e) {
            AppLog.error(e);
        }
    }

}
