package com.mbc.tool.rest.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    public String msisdn;
    public String imsi;
    public String iccid;
    public String a4ki;

    public String simSerial;
}
