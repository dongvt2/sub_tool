package com.mbc.tool.rest;

import com.mbc.tool.rest.base.Validator;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class BaseResponse {
    String refNo;
    Validator.Result result;

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Validator.Result getResult() {
        return result;
    }

    public void setResult(Validator.Result result) {
        if (result != null && result.getMessage() != null) {
//            result.setMessage(Utility.removeAccent(result.getMessage()));
        }
        this.result = result;
    }

    @Override
    public String toString() {
        String fullResponse = ToStringBuilder.reflectionToString(this, new RecursiveToStringStyle());
        return fullResponse;
    }
}
