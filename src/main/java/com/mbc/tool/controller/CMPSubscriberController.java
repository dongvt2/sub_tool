package com.mbc.tool.controller;

import com.mbc.tool.rest.SubscriberCancelRequest;
import com.mbc.tool.rest.SubscriberCancelResponse;
import com.mbc.tool.rest.base.Request;
import javax.validation.Validator;
import com.mbc.tool.service.base.SubcriberServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@Validated
@RequestMapping(path = "/subscriber")
public class CMPSubscriberController extends BaseController{

    public CMPSubscriberController(Validator validator) {
        super(validator);
    }

    @Autowired
    SubcriberServiceBase subcriberServiceBase;

    @PostMapping("/cancel")
    public SubscriberCancelResponse cancel(@RequestBody SubscriberCancelRequest param){
        Request request = new Request();
        request.setImsi(param.getImsi());
        request.setMsisdn(param.getMsisdn());
        Object custObject = null;
        return subcriberServiceBase.subcriberCancel(request, custObject);
    }
}
