/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbc.tool.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.Utils.Constant;
import com.mbc.tool.rest.base.Validator.Result;
import com.mbc.tool.rest.base.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dongvt
 */
@RestController
public class BaseController {

    protected @Context
    UriInfo uriInfo;
    protected @Context
    @Autowired
    HttpServletRequest httpRequest;
    public final Validator validator;
    public BaseController(Validator validator) {
        this.validator = validator;
    }

    public Result validateServicePath(BaseRequest baseRequest) {
        Result result = Result.OK;
        //String uriPath = "/" + httpRequest.getContextPath();

//        if (!DataCachedCollection.checkServicePath(uriPath)) {
//            result = new SimpleResult("Service invalid", false, ResponseCode.SERVICE_ISNT_SUPPORTED);
//        }
//        if (result.isOk()) {
//            boolean isValid = false;
//            if (DataCachedCollection.checkServiceNonSession(uriPath)) {
//                isValid = true;
//            }
//            // hardcode: kiem tra them trong qua trinh chay live
//            if (!uriPath.contains("/common/updateRefNumber") || !uriPath.contains("/common/checkPhoneNoExistedAndSendOTP")) {
//                isValid = true;
//            }
//            SessionData sessionData = SessionFilter.getInstance().getSessionById(baseRequest.getSessionId());
//            Cust cust = null;
//            if (!isValid && sessionData != null) {
//                cust = sessionData.getCust();
//                if (cust != null && !RedisServer.isDuplicateExecutedUri(cust.getUserId(), uriPath)) {
//                    isValid = true;
//                }
//            }
//            isValid = validateSomeSpecificServicePath(baseRequest, uriPath, isValid, sessionData);
//            if (!isValid) {
//                result = new SimpleResult("You're trying call our server with same uri in the same time. Please try next time.", false, ResponseCode.DDOS_SAME_TIME);
//                saveBlockedTransactionLogs(baseRequest, cust, result);
//            }
//        }
        return result;
    }
//

    public Result validateInput(BaseRequest request) {
        Result result = Result.OK;
        return result;
    }
//

    public List<String> validateRequest(Set<ConstraintViolation<Object>> violations) {
        List<String> result = new ArrayList<>();
//        if (violations.size() > 0) {
//            for (ConstraintViolation<Object> violation : violations) {
//                result.add(violation.getPropertyPath().toString() + ": " + violation.getMessage());
//            }
//        }
        return result;
    }


//

    public Result validate(BaseRequest request) {
       Result result = Result.OK;

        if (result.isOk()) {
            result = validateInput(request);
        }
        return result;
    }


    public Request setBase(Request request, BaseRequest param) {
        request.setUrlPath(httpRequest.getRequestURI());
        request.setRequestContent(param.toString());
        request.setIpAddress(httpRequest.getRemoteAddr());
        String userAgent = httpRequest.getHeader("User-Agent") == null ? Constant.EMPTY_STR : httpRequest.getHeader("User-Agent");
        request.setHeaderAgent(userAgent);
        request.setIpServer(httpRequest.getLocalAddr());
        return request;
    }
//

    public Result validateConnect() {
        Result result = Result.OK;
        if (result.isOk()) {
            String agent = httpRequest.getHeader("User-Agent");
            AppLog.info("User-Agent: " + agent);
        }
        return result;
    }
}
