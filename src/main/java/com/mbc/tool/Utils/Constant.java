package com.mbc.tool.Utils;

public class Constant {
    public static final String EMPTY_STR="";

    public static final String POST ="POST";
    public static final String PUT ="PUT";
    public static final String GET ="GET";
    public static final String DELETE ="DELETE";
    public static final String DATA_JSON ="data_json";
}
