package com.mbc.tool.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class AppLog {
    public Logger logger = LogManager.getLogger(this.getClass());

    static {
//        logger = LogManager.getLogger(this.getClass());
    }

    public static void info(String logString) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.info(logString);

    }

    public static void error(String logString) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.error( ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber() + " - " + logString);

    }

    public static void error(Exception e) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.error(ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber() + " - " + e);
        subLog.error("", e);

    }

    public static void error(String info, Exception e) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.error(ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber() + " - " + e);
        subLog.error(info, e);

    }

    public static void fatal(String logString) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.fatal(ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber() + " - " + logString);
    }

    public static void warning(String logString) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String clsName = ste.getClassName();
        Logger subLog = LogManager.getLogger(clsName);
        if (subLog == null) {
            return;
        }
        subLog.warn(ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber() + " - " + logString);
    }

    private static String getStackTrace(Exception e) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            if (e != null) {
                sw = new StringWriter();
                pw = new PrintWriter(sw, true);
                e.printStackTrace(pw);
                return sw.getBuffer().toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (pw != null) {
                    pw.close();
                }
                if (sw != null) {

                    sw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return "";
    }

    private static void sendKibanaLog(String logLevel, String logString) {
//        if (Constant.LOG_LEVEL_INFO.equals(logLevel)) {
////            KibanaLog.send(new ObjectElasticLog(null, 0, Constant.LOG_LEVEL_INFO, Constant.CHANNEL_EMB, Constant.EMPTY_STR, Constant.EMPTY_STR,
////                    Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, logString, null));
//        } else if (Constant.LOG_LEVEL_ERROR.equals(logLevel)) {
////            KibanaLog.send(new ObjectElasticLog(null, 0, Constant.LOG_LEVEL_ERROR, Constant.CHANNEL_EMB, Constant.EMPTY_STR, Constant.EMPTY_STR,
////                    Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, Constant.EMPTY_STR, logString, null));
//        } else {
//            //do nothing
//        }
    }
}
