package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WsResponse {
    List<HlrInfo> hlrResponse;
}
