package com.mbc.tool.cmp.bean;



import com.mbc.tool.rest.BaseResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class HlrResponse extends ResponseCmpBase {

    WsResponse wsResponse;
}
