package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HlrInfo {

    public String traceId;
    public String userId;
    public String code;
    public String message;
    public HlrDetail detail;
}
