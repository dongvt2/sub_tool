package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CmpSubscriberCancelInput {
    String msisdn;
    String imsi;
    String iccid;
    String a4ki;

    public CmpSubscriberCancelInput(String msisdn, String imsi, String iccid, String a4ki) {
        this.msisdn = msisdn;
        this.imsi = imsi;
        this.iccid = iccid;
        this.a4ki = a4ki;
    }

    public CmpSubscriberCancelInput() {
    }
}
