package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Imsi {
    String imsi;

    public Imsi(String imsi) {
        this.imsi = imsi;
    }
}
