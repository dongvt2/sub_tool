package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HlrDetail {
    String cmd;
    String resp;
}
