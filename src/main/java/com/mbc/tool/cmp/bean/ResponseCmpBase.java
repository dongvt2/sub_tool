package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseCmpBase {
    public String code;
    public String message;
}
