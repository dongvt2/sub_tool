package com.mbc.tool.cmp.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestCmpBase {
    private String username;
    private String token;
    private Object wsRequest;
}
