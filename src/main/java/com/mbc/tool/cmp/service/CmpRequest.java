package com.mbc.tool.cmp.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbc.tool.Utils.AppLog;
import com.mbc.tool.Utils.Constant;
import com.mbc.tool.cmp.bean.CmpSubscriberCancelInput;
import com.mbc.tool.cmp.bean.HlrResponse;
import com.mbc.tool.cmp.bean.Imsi;
import com.mbc.tool.cmp.bean.RequestCmpBase;
import com.mbc.tool.config.CmpConfig;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Service
public class CmpRequest {

    public String sendRequest(Map<String, String> input, CmpUrlConstant urlPath) {
        String response = "";
        try {
            Map<String, String> mapMessage = new HashMap();
            if (input != null) {
                mapMessage.putAll(input);
            }
        } catch (Exception e) {
            AppLog.error(e);
        }
        if (urlPath.getMethod().equals(Constant.POST) || urlPath.getMethod().equals(Constant.PUT)) {
            String datapost = input.get(Constant.DATA_JSON);
            try {
                response = this.postRequest(datapost, urlPath, urlPath.getMethod());
            } catch (Exception e) {
                AppLog.error(e);
            }
        } else if (urlPath.getMethod().equals(Constant.GET)) {
            response = this.getRequest(input, urlPath);
        } else if (urlPath.getMethod().equals(Constant.DELETE)) {
//            response = this.deleteRequest(input, urlPath);
        }
        AppLog.info("Response :: " + response);
        return response;
    }

    public String sendRequest(Map<String, String> input, CmpUrlConstant urlPath, String planningToken) {
        String response = "";
        try {
            Map<String, String> mapMessage = new HashMap();
            if (input != null) {
                mapMessage.putAll(input);
            }
        } catch (Exception e) {
            AppLog.error(e);
        }
        if (urlPath.getMethod().equals(Constant.POST) || urlPath.getMethod().equals(Constant.PUT)) {
            String datapost = input.get(Constant.DATA_JSON);
            try {
                response = this.postRequest(datapost, urlPath, urlPath.getMethod());
            } catch (Exception e) {
                AppLog.error(e);
            }
        } else if (urlPath.getMethod().equals(Constant.GET)) {
            response = this.getRequest(input, urlPath);
        } else if (urlPath.getMethod().equals(Constant.DELETE)) {
//            response = this.deleteRequest(input, urlPath);
        }

        return response;
    }

    private String postRequest(String datapost, CmpUrlConstant urlPath, String method) {
        RestTemplate restTemplate = new RestTemplate();
        String output = "";
        final String baseUrl = CmpConfig.URL + urlPath.getUrl();
        URI uri;

        try {
            HttpHeaders headers = new HttpHeaders();
            // Please do not change this submission method. In most cases, the submission
            // method is form submission
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.set("Authorization", "Bearer " + this.athen.getAccess_token());

            HttpEntity<String> entity = new HttpEntity<String>(datapost, headers);
            // Just replace the exchange method
            if (Constant.POST.equals(method)) {
                ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.POST, entity, String.class);
                return response.getBody();
            } else if (Constant.PUT.equals(method)) {
                ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, String.class);
                return response.getBody();
            } else {
                return "";
            }
        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            if (statusCode == 401) {
//                athen = getToken();
                this.postRequest(datapost, urlPath, method);
            }
        }
        return output;
    }


    private String getRequest(Map<String, String> input, CmpUrlConstant urlPath) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        String output = "";
        final String baseUrl = CmpConfig.URL + urlPath.getUrl();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.set("Authorization", "Bearer " + this.athen.getAccess_token());

            // Package parameters. Do not replace them with map and HashMap, otherwise the
            // parameters cannot be passed
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // Just replace the exchange method
            ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.GET, entity, String.class,
                    input);

            return response.getBody();
        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            if (statusCode == 401) {
//                athen = getToken();
                this.getRequest(input, urlPath);
            }
            AppLog.error(exception);
        }
        return output;
    }

    /**
     * lay thong tin hlr
     * @param imsi
     * @return
     */
    public HlrResponse getInfoHrlHss(String imsi){
        HlrResponse response = null;
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        RequestCmpBase input = new RequestCmpBase();
        input.setUsername("dainv");
        input.setToken("abc");
        input.setWsRequest(new Imsi(imsi));
        Map<String, String> inputQuery = new HashMap<String, String>();
        try {
            inputQuery.put(Constant.DATA_JSON, mapper.writeValueAsString(input));
            String json = this.sendRequest(inputQuery,CmpUrlConstant.SUBSCRIBER_INFO);

            response = mapper.readValue(json,HlrResponse.class);

        }catch (Exception ex){
            AppLog.error(ex);
        }
        return response;
    }

    /**
     * huy thue bao cmp
     * @param cmpInfo
     * @return
     */
    public HlrResponse cmpSubscriberCancel (CmpSubscriberCancelInput cmpInfo){
        HlrResponse response = null;
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        RequestCmpBase input = new RequestCmpBase();
        input.setUsername("685QOX");
        input.setToken("I1YDKJATCB");
        input.setWsRequest(cmpInfo);
        Map<String, String> inputQuery = new HashMap<String, String>();
        try {
            inputQuery.put(Constant.DATA_JSON, mapper.writeValueAsString(input));
            String json = this.sendRequest(inputQuery,CmpUrlConstant.CMP_REMOVE_SUB);
            response = mapper.readValue(json,HlrResponse.class);
            AppLog.info(json);
        }catch (Exception ex){
            AppLog.error(ex);
        }
        return response;
    }


    /**
     * huy thue bao hlr
     * @param cmpInfo
     * @return
     */
    public HlrResponse hlrSubscriberCancel (CmpSubscriberCancelInput cmpInfo){
        HlrResponse response = null;
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        RequestCmpBase input = new RequestCmpBase();
        input.setUsername("dainv");
        input.setToken("abc");
        input.setWsRequest(cmpInfo);
        Map<String, String> inputQuery = new HashMap<String, String>();
        try {
            inputQuery.put(Constant.DATA_JSON, mapper.writeValueAsString(input));
            String json = this.sendRequest(inputQuery,CmpUrlConstant.HLR_REMOVE_SUB);
            response = mapper.readValue(json,HlrResponse.class);
            AppLog.info(json);
        }catch (Exception ex){
            AppLog.error(ex);
        }
        return response;
    }

    /**
     * huy thue bao hlr
     * @param cmpInfo
     * @return
     */
    public HlrResponse hlrSubscriberCancel4G (CmpSubscriberCancelInput cmpInfo){
        HlrResponse response = null;
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        RequestCmpBase input = new RequestCmpBase();
        input.setUsername("dainv");
        input.setToken("abc");
        input.setWsRequest(cmpInfo);
        Map<String, String> inputQuery = new HashMap<String, String>();
        try {
            inputQuery.put(Constant.DATA_JSON, mapper.writeValueAsString(input));
            String json = this.sendRequest(inputQuery,CmpUrlConstant.HLR_REMOVE_4G);
            response = mapper.readValue(json,HlrResponse.class);
            AppLog.info(json);
        }catch (Exception ex){
            AppLog.error(ex);
        }
        return response;
    }
}
