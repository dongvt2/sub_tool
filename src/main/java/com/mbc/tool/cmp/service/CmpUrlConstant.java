package com.mbc.tool.cmp.service;

public enum CmpUrlConstant {

    SUBSCRIBER_INFO("POST","/cmp-provisioning/api/v1/subscriber/getSubscriberInfo","Get  Subscriber Info "),

    CMP_REMOVE_SUB("POST","/cmp-provisioning/api/v1/subscriber/cancel","Hủy thuê bao CMP"),

    HLR_REMOVE_SUB("POST","/cmp-provisioning/api/v1/hlr/removeSubscriber","Xóa thuê bao trên HLR "),
    HLR_REMOVE_4G("POST","/cmp-provisioning/api/v1/hlr/remove4G","Hủy thuê bao 4G "),


    ;

    CmpUrlConstant(String method, String url, String description) {
        this.method = method;
        this.url = url;
        this.description = description;
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    String method;
    String url;
    String description;
}
