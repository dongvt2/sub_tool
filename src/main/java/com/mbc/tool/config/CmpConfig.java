package com.mbc.tool.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@Configuration
@PropertySources({
        @PropertySource("classpath:application.properties")})
public class CmpConfig {

    @Autowired
    private Environment env; // Contains Properties Load by @PropertySources

    public static String URL;
    public static String USER_NAME;
    public static String PASSWORD;

    @PostConstruct
    public void init() {
        URL = env.getProperty("cmp.url");
        USER_NAME = env.getProperty("cmp.user");
        PASSWORD = env.getProperty("cmp.pass");
    }
}
