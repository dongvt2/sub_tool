package com.mbc.tool.config.database;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.mbc.tool.repositories.inventory", entityManagerFactoryRef = "inventoryEntityManagerFactory", transactionManagerRef = "inventoryTransactionManager")

public class DataSourceInventoryConfig {
    @Bean
    @ConfigurationProperties("spring.inventorydatasource")
    public DataSourceProperties inDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.inventorydatasource.configuration")
    public DataSource inventoryDataSource() {
        return inDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean(name = "inventoryEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean inventoryEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(inventoryDataSource()).packages("com.mbc.tool.model.inventory").build();
    }

    @Bean(name = "inventoryTransactionManager")
    public PlatformTransactionManager inventoryTransactionManager(
            final @Qualifier("inventoryEntityManagerFactory") LocalContainerEntityManagerFactoryBean inventoryEntityManagerFactory) {
        return new JpaTransactionManager(inventoryEntityManagerFactory.getObject());
    }
}