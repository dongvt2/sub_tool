package com.mbc.tool.config.database;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.mbc.tool.repositories.cust", entityManagerFactoryRef = "custEntityManagerFactory", transactionManagerRef = "custTransactionManager")
public class DataSourceCustConfig {
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties custDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.configuration")
    public DataSource custDataSource() {
        return custDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean(name = "custEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean custEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(custDataSource()).packages("com.mbc.tool.model.cust").build();
    }

    @Bean(name = "custTransactionManager")
    public PlatformTransactionManager custTransactionManager(
            final @Qualifier("custEntityManagerFactory") LocalContainerEntityManagerFactoryBean custEntityManagerFactory) {
        return new JpaTransactionManager(custEntityManagerFactory.getObject());
    }
}
