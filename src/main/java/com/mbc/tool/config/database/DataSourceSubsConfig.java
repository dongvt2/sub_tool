package com.mbc.tool.config.database;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.mbc.tool.repositories.subscriber", entityManagerFactoryRef = "subEntityManagerFactory", transactionManagerRef = "subTransactionManager")
public class DataSourceSubsConfig {
    @Bean
    @Primary
    @ConfigurationProperties("spring.subscriberdatasource")
    public DataSourceProperties subsDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("spring.subscriberdatasource.configuration")
    public DataSource subsDataSource() {
        return subsDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    /*Primary Entity manager*/
    @Primary
    @Bean(name = "subEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean subEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(subsDataSource()).packages("com.mbc.tool.model.subscriber").build();
    }

    @Primary
    @Bean
    public PlatformTransactionManager subTransactionManager(
            final @Qualifier("subEntityManagerFactory") LocalContainerEntityManagerFactoryBean subEntityManagerFactory) {
        return new JpaTransactionManager(subEntityManagerFactory.getObject());
    }
}
