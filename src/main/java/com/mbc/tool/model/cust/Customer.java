package com.mbc.tool.model.cust;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "customer")
public class Customer {
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private Long city;

    @Column(name = "country")
    private Long country;

    @Column(name = "cust_code")
    private String custCode;

    @Column(name = "cust_name")
    private String custName;

    @Column(name = "district")
    private Long district;

    @Column(name = "gender")
    private Boolean gender;

    @Column(name = "language")
    private String language;

    @Column(name = "phone")
    private String phone;

    @Column(name = "receive_notifi_ads")
    private String receiveNotifiAds;

    @Column(name = "village")
    private String village;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "cust_type")
    private Long custType;

    @Id
    @Column(name = "cust_id")
    private Long custId;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "email")
    private String email;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "tax_code")
    private String taxCode;

    @Column(name = "path")
    private String path;

    @Column(name = "identify_number")
    private String identifyNumber;

    @Column(name = "identify_type")
    private String identifyType;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "isdn_registed")
    private String isdnRegisted;

    @Column(name = "subscriber")
    private String subscriber;

    @Column(name = "description")
    private String description;

    @Column(name = "number_employee")
    private Long numberEmployee;

    @Column(name = "trust_text_percent")
    private Long trustTextPercent;

    @Column(name = "trust_image_percent")
    private Long trustImagePercent;

    @Column(name = "group_id")
    private String groupId;

    @Column(name = "kyc_job")
    private Long kycJob;

    @Column(name = "avatar_reddi")
    private String avatarReddi;

    @Column(name = "is_trust")
    private Boolean isTrust;

    @Column(name = "is_send_to_vcm")
    private Long isSendToVcm;

    @Column(name = "update_at")
    private Date updateAt;

    @Column(name = "is_masan_customer")
    private Boolean isMasanCustomer;

}
