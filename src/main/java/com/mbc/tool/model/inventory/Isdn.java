package com.mbc.tool.model.inventory;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "isdn")
public class Isdn {
    @Id
    @Column(name = "isdn")
    private String isdn;

    @Column(name = "create_at")
    private Date createAt;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "type_id")
    private Long typeId;

    @Column(name = "price_custom")
    private Long priceCustom;

    @Column(name = "price_default")
    private Long priceDefault;

    @Column(name = "role_id")
    private Long roleId;

    @Column(name = "shop_id")
    private Long shopId;

    @Column(name = "update_at")
    private Date updateAt;

    @Column(name = "object_holding")
    private Boolean objectHolding;

    @Column(name = "sold")
    private Boolean sold;

    @Column(name = "level")
    private Long level;

    @Column(name = "version")
    private Long version;

    @Column(name = "mnp")
    private Boolean mnp;

    @Column(name = "session_keeping")
    private String sessionKeeping;

    @Column(name = "part")
    private Long part;

    @Column(name = "reverse")
    private Long reverse;

    @Column(name = "aisdn")
    private String aisdn;

}
