package com.mbc.tool.model.inventory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "kit")
public class Kit implements Serializable {
    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "creator")
    private String creator;

    @Column(name = "description")
    private String description;

    @Id
    @Column(name = "im_serial")
    private String imSerial;

    @Column(name = "isdn")
    private String isdn;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "package_id")
    private Long packageId;

    @Column(name = "shop_id")
    private Long shopId;

    @Column(name = "subpackage_id")
    private Long subpackageId;

    @Column(name = "price")
    private Long price;

    @Column(name = "object_holding")
    private Boolean objectHolding;

    @Column(name = "level")
    private Long level;

    @Column(name = "sold")
    private Long sold;

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImSerial() {
        return this.imSerial;
    }

    public void setImSerial(String imSerial) {
        this.imSerial = imSerial;
    }

    public String getIsdn() {
        return this.isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getPackageId() {
        return this.packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public Long getShopId() {
        return this.shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getSubpackageId() {
        return this.subpackageId;
    }

    public void setSubpackageId(Long subpackageId) {
        this.subpackageId = subpackageId;
    }

    public Long getPrice() {
        return this.price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Boolean getObjectHolding() {
        return this.objectHolding;
    }

    public void setObjectHolding(boolean objectHolding) {
        this.objectHolding = objectHolding;
    }

    public Long getLevel() {
        return this.level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getSold() {
        return this.sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }
}
