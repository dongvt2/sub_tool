package com.mbc.tool.model.inventory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sim")
public class Sim implements Serializable {
    @Id
    @Column(name = "im_serial")
    private String imSerial;

    @Column(name = "authen_sim")
    private Boolean authenSim;

    @Column(name = "code_adm")
    private String codeAdm;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "creator")
    private String creator;

    @Column(name = "description")
    private String description;

    @Column(name = "esim_qrcode")
    private String esimQrcode;

    @Column(name = "iccid")
    private String iccid;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "ipa_string")
    private String ipaString;

    @Column(name = "ki")
    private String ki;

    @Column(name = "level")
    private Long level;

    @Column(name = "object_holding")
    private Long objectHolding;

    @Column(name = "pin")
    private String pin;

    @Column(name = "pin2")
    private String pin2;

    @Column(name = "puk")
    private String puk;

    @Column(name = "puk2")
    private String puk2;

    @Column(name = "shop_id")
    private Long shopId;

    @Column(name = "sim_reference")
    private String simReference;

    @Column(name = "sold")
    private Long sold;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "warehouse_entry_code")
    private String warehouseEntryCode;

    @Column(name = "price_default")
    private Long priceDefault;

    public String getImSerial() {
        return this.imSerial;
    }

    public void setImSerial(String imSerial) {
        this.imSerial = imSerial;
    }

    public Boolean getAuthenSim() {
        return this.authenSim;
    }

    public void setAuthenSim(Boolean authenSim) {
        this.authenSim = authenSim;
    }

    public String getCodeAdm() {
        return this.codeAdm;
    }

    public void setCodeAdm(String codeAdm) {
        this.codeAdm = codeAdm;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEsimQrcode() {
        return this.esimQrcode;
    }

    public void setEsimQrcode(String esimQrcode) {
        this.esimQrcode = esimQrcode;
    }

    public String getIccid() {
        return this.iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getImsi() {
        return this.imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getIpaString() {
        return this.ipaString;
    }

    public void setIpaString(String ipaString) {
        this.ipaString = ipaString;
    }

    public String getKi() {
        return this.ki;
    }

    public void setKi(String ki) {
        this.ki = ki;
    }

    public Long getLevel() {
        return this.level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getObjectHolding() {
        return this.objectHolding;
    }

    public void setObjectHolding(Long objectHolding) {
        this.objectHolding = objectHolding;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPin2() {
        return this.pin2;
    }

    public void setPin2(String pin2) {
        this.pin2 = pin2;
    }

    public String getPuk() {
        return this.puk;
    }

    public void setPuk(String puk) {
        this.puk = puk;
    }

    public String getPuk2() {
        return this.puk2;
    }

    public void setPuk2(String puk2) {
        this.puk2 = puk2;
    }

    public Long getShopId() {
        return this.shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getSimReference() {
        return this.simReference;
    }

    public void setSimReference(String simReference) {
        this.simReference = simReference;
    }

    public Long getSold() {
        return this.sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }

    public Long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getWarehouseEntryCode() {
        return this.warehouseEntryCode;
    }

    public void setWarehouseEntryCode(String warehouseEntryCode) {
        this.warehouseEntryCode = warehouseEntryCode;
    }

    public Long getPriceDefault() {
        return this.priceDefault;
    }

    public void setPriceDefault(Long priceDefault) {
        this.priceDefault = priceDefault;
    }
}
