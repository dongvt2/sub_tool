package com.mbc.tool.model.sale;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "direct_order")
public class DirectOrder {
    @Column(name = "im_serial")
    private String imSerial;

    @Column(name = "product_type")
    private Long productType;

    @Column(name = "main_pack_id")
    private Long mainPackId;

    @Column(name = "sub_pack_id")
    private Long subPackId;

    @Column(name = "cust_phone")
    private String custPhone;

    @Column(name = "cust_name")
    private String custName;

    @Column(name = "total")
    private Long total;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "basic_pack_id")
    private Long basicPackId;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "shop_id")
    private Long shopId;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "is_kyc")
    private Long isKyc;

    @Column(name = "call_amount")
    private Long callAmount;

    @Column(name = "data_amount")
    private Long dataAmount;

    @Column(name = "sms_amount")
    private Long smsAmount;

    @Column(name = "isdn")
    private String isdn;

    @Column(name = "isdn_type")
    private Long isdnType;

    @Column(name = "email")
    private String email;

    @Column(name = "purchase_order_id")
    private Long purchaseOrderId;

    @Column(name = "qr_code")
    private String qrCode;

    @Column(name = "isdn_price")
    private Long isdnPrice;

    @Column(name = "package_price")
    private Long packagePrice;

    @Column(name = "update_at")
    private Date updateAt;

    @Column(name = "sim_price")
    private Long simPrice;

    @Column(name = "e_sim")
    private Boolean eSim;

    @Column(name = "esim_qrcode")
    private String esimQrcode;

    public String getImSerial() {
        return this.imSerial;
    }

    public void setImSerial(String imSerial) {
        this.imSerial = imSerial;
    }

    public Long getProductType() {
        return this.productType;
    }

    public void setProductType(Long productType) {
        this.productType = productType;
    }

    public Long getMainPackId() {
        return this.mainPackId;
    }

    public void setMainPackId(Long mainPackId) {
        this.mainPackId = mainPackId;
    }

    public Long getSubPackId() {
        return this.subPackId;
    }

    public void setSubPackId(Long subPackId) {
        this.subPackId = subPackId;
    }

    public String getCustPhone() {
        return this.custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustName() {
        return this.custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Long getTotal() {
        return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getBasicPackId() {
        return this.basicPackId;
    }

    public void setBasicPackId(Long basicPackId) {
        this.basicPackId = basicPackId;
    }

    public Long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getShopId() {
        return this.shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getIsKyc() {
        return this.isKyc;
    }

    public void setIsKyc(Long isKyc) {
        this.isKyc = isKyc;
    }

    public Long getCallAmount() {
        return this.callAmount;
    }

    public void setCallAmount(Long callAmount) {
        this.callAmount = callAmount;
    }

    public Long getDataAmount() {
        return this.dataAmount;
    }

    public void setDataAmount(Long dataAmount) {
        this.dataAmount = dataAmount;
    }

    public Long getSmsAmount() {
        return this.smsAmount;
    }

    public void setSmsAmount(Long smsAmount) {
        this.smsAmount = smsAmount;
    }

    public String getIsdn() {
        return this.isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Long getIsdnType() {
        return this.isdnType;
    }

    public void setIsdnType(Long isdnType) {
        this.isdnType = isdnType;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPurchaseOrderId() {
        return this.purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public String getQrCode() {
        return this.qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Long getIsdnPrice() {
        return this.isdnPrice;
    }

    public void setIsdnPrice(Long isdnPrice) {
        this.isdnPrice = isdnPrice;
    }

    public Long getPackagePrice() {
        return this.packagePrice;
    }

    public void setPackagePrice(Long packagePrice) {
        this.packagePrice = packagePrice;
    }

    public Date getUpdateAt() {
        return this.updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Long getSimPrice() {
        return this.simPrice;
    }

    public void setSimPrice(Long simPrice) {
        this.simPrice = simPrice;
    }

    public Boolean getESim() {
        return this.eSim;
    }

    public void setESim(Boolean eSim) {
        this.eSim = eSim;
    }

    public String getEsimQrcode() {
        return this.esimQrcode;
    }

    public void setEsimQrcode(String esimQrcode) {
        this.esimQrcode = esimQrcode;
    }
}
