package com.mbc.tool.model.sale;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "retail_order" , schema = "sale")
public class RetailOrder {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "call_amount")
    private Long callAmount;

    @Column(name = "code")
    private String code;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "data_amount")
    private Long dataAmount;

    @Column(name = "isdn")
    private String isdn;

    @Column(name = "package_code")
    private String packageCode;

    @Column(name = "sms_amount")
    private Long smsAmount;

    @Column(name = "status")
    private Long status;

    @Column(name = "type")
    private Long type;

    @Column(name = "is_kyc")
    private Long isKyc;

    @Column(name = "sim_serial")
    private String simSerial;

    @Column(name = "app_code")
    private String appCode;

    @Column(name = "payment_status")
    private Long paymentStatus;

    @Column(name = "package_id")
    private Long packageId;

    @Column(name = "lang")
    private String lang;

    @Column(name = "export_type")
    private Long exportType;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "exporter")
    private String exporter;

    @Column(name = "isdn_price")
    private Long isdnPrice;

    @Column(name = "package_price")
    private Long packagePrice;

    @Column(name = "qr_code")
    private String qrCode;

    @Column(name = "isdn_type")
    private Long isdnType;

    @Column(name = "e_sim")
    private String eSim;

    @Column(name = "esim_qrcode")
    private String esimQrcode;

    @Column(name = "masaner_phone")
    private String masanerPhone;

    @Column(name = "sim_price")
    private Long simPrice;

    @Column(name = "old_sim_serial")
    private String oldSimSerial;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCallAmount() {
        return this.callAmount;
    }

    public void setCallAmount(Long callAmount) {
        this.callAmount = callAmount;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getDataAmount() {
        return this.dataAmount;
    }

    public void setDataAmount(Long dataAmount) {
        this.dataAmount = dataAmount;
    }

    public String getIsdn() {
        return this.isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getPackageCode() {
        return this.packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public Long getSmsAmount() {
        return this.smsAmount;
    }

    public void setSmsAmount(Long smsAmount) {
        this.smsAmount = smsAmount;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return this.type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getIsKyc() {
        return this.isKyc;
    }

    public void setIsKyc(Long isKyc) {
        this.isKyc = isKyc;
    }

    public String getSimSerial() {
        return this.simSerial;
    }

    public void setSimSerial(String simSerial) {
        this.simSerial = simSerial;
    }

    public String getAppCode() {
        return this.appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public Long getPaymentStatus() {
        return this.paymentStatus;
    }

    public void setPaymentStatus(Long paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Long getPackageId() {
        return this.packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Long getExportType() {
        return this.exportType;
    }

    public void setExportType(Long exportType) {
        this.exportType = exportType;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getExporter() {
        return this.exporter;
    }

    public void setExporter(String exporter) {
        this.exporter = exporter;
    }

    public Long getIsdnPrice() {
        return this.isdnPrice;
    }

    public void setIsdnPrice(Long isdnPrice) {
        this.isdnPrice = isdnPrice;
    }

    public Long getPackagePrice() {
        return this.packagePrice;
    }

    public void setPackagePrice(Long packagePrice) {
        this.packagePrice = packagePrice;
    }

    public String getQrCode() {
        return this.qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Long getIsdnType() {
        return this.isdnType;
    }

    public void setIsdnType(Long isdnType) {
        this.isdnType = isdnType;
    }

    public String getESim() {
        return this.eSim;
    }

    public void setESim(String eSim) {
        this.eSim = eSim;
    }

    public String getEsimQrcode() {
        return this.esimQrcode;
    }

    public void setEsimQrcode(String esimQrcode) {
        this.esimQrcode = esimQrcode;
    }

    public String getMasanerPhone() {
        return this.masanerPhone;
    }

    public void setMasanerPhone(String masanerPhone) {
        this.masanerPhone = masanerPhone;
    }

    public Long getSimPrice() {
        return this.simPrice;
    }

    public void setSimPrice(Long simPrice) {
        this.simPrice = simPrice;
    }

    public String getOldSimSerial() {
        return this.oldSimSerial;
    }

    public void setOldSimSerial(String oldSimSerial) {
        this.oldSimSerial = oldSimSerial;
    }
}
