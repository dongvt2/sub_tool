package com.mbc.tool.model.subscriber;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "subscriber")
public class Subscriber {
    @Id
    @Column(name = "sub_id")
    private Long subId;

    @Column(name = "activated_at")
    private String activatedAt;

    @Column(name = "call_direct")
    private String callDirect;

    @Column(name = "created_at")
    private String createdAt;

    @Column(name = "isdn")
    private String isdn;

    @Column(name = "m_main")
    private Long mMain;

    @Column(name = "m_expired_at")
    private String mExpiredAt;

    @Column(name = "m_promotion")
    private Long mPromotion;

    @Column(name = "sim_serial")
    private String simSerial;

    @Column(name = "type")
    private String type;

    @Column(name = "cust_id")
    private Long custId;

    @Column(name = "status")
    private Long status;

    @Column(name = "blocked_at")
    private String blockedAt;

    @Column(name = "call_in")
    private Long callIn;

    @Column(name = "call_out")
    private Long callOut;

    @Column(name = "amt_call_in")
    private Long amtCallIn;

    @Column(name = "amt_call_out")
    private Long amtCallOut;

    @Column(name = "amt_data")
    private Long amtData;

    @Column(name = "amt_sms_in")
    private Long amtSmsIn;

    @Column(name = "amt_sms_out")
    private Long amtSmsOut;

    @Column(name = "main_pack_id")
    private Long mainPackId;

    @Column(name = "sub_type")
    private Long subType;

    @Column(name = "sub_pakage_id")
    private Long subPakageId;

    @Column(name = "update_at")
    private Date updateAt;

    @Column(name = "imei")
    private String imei;

    @Column(name = "last_update_user")
    private String lastUpdateUser;

    @Column(name = "approve_date")
    private Date approveDate;

    @Column(name = "approve_user")
    private String approveUser;

    @Column(name = "approve_status_id")
    private Boolean approveStatusId;

    @Column(name = "audit_status_id")
    private Boolean auditStatusId;

    @Column(name = "count_rekyc")
    private Long countRekyc;

    @Column(name = "e_sim")
    private Boolean eSim;

    @Column(name = "club_flag")
    private Boolean clubFlag;

    @Column(name = "keep_send_vc")
    private Boolean keepSendVc;

    @Column(name = "group_type")
    private Boolean groupType;

}
