package com.mbc.tool.chainofresponsibility;

import com.mbc.tool.rest.base.Validator;

public abstract  class SubscriberCancel {
    protected SubscriberCancel subscriber;

    // check
    protected abstract boolean canHander(Validator.Result result);
    protected abstract Validator.Result checkHlr();
    protected abstract Validator.Result cancleCmp();
    protected abstract Validator.Result cancleHlr();
}
